# Importing necessary modules and parts.

from math import cos, sin, pi
import numpy as np
import matplotlib.pyplot as plt
import sys

# Define the trigonometric functions.

"""
The first of the three trigonometric functions.
"""
def X(t, Ax, fx):
    return Ax * cos(2 * pi * fx * t)

"""
The second of the three trigonometric functions.
"""
def Y(t, Ay, fy, phi):
    return Ay * sin(2 * pi * fy * t + phi)

"""
The third of the three trigonometric functions. It is just the sum of the
previous two.
"""
def Z(t, Ax, Ay, fx, fy, phi):
    return X(t, Ax, fx) + Y(t, Ay, fy, phi)

# Define the runtime for computing the sequence.

"""
Computes the N data points, given the physical parameters and time step
supplied. Returns a 4-tuple of Python lists, where each list is one set
of N data points for each of the three functions: X, Y, and Z, in that
order. The first list, before these three, is the time at which those
points were taken.
"""
def compute_sequence(delta_t, N, Ax, Ay, fx, fy, phi):
    (t_out, x_out, y_out, z_out) = (
        [None] * N,
        [None] * N,
        [None] * N,
        [None] * N
    )
    for n in range(N):
        t = delta_t * n
        (t_out[n], x_out[n], y_out[n], z_out[n]) = (
        t,
        X(t, Ax, fx),
        Y(t, Ay, fy, phi),
        Z(t, Ax, Ay, fx, fy, phi)
        )
    return (t_out, x_out, y_out, z_out)

# Define functions for saving the computed data.

"""
Expects the 4-tuple of lists from the compute_sequence() function.
Saves the data using a Python file to a file with the name specified
in the DEFAULT_LIST_SAVE_STRING variable.
"""
def save_as_list(data, save_to):
    (t_out, x_out, y_out, z_out) = data
    with open(save_to, 'w') as out:
        out.write(str(t_out))
        out.write(str(x_out))
        out.write(str(y_out))
        out.write(str(z_out))

"""
Takes the output from compute_sequence() and converts it into a 4-tuple of
numpy arrays.
"""
def as_numpy_array(txyz_out):
    return (
    np.array(txyz_out[0]),
    np.array(txyz_out[1]),
    np.array(txyz_out[2]),
    np.array(txyz_out[3]),
    )

"""
Expects the 4-tuple of lists from the compute_sequence() function.
Saves the data using the numpy.savetxt() function, to a file with the
name specified in the DEFAULT_ARRAY_SAVE_STRING variable.
"""
def save_as_array(data, save_to):
    data = as_numpy_array(data)
    np.savetxt(save_to, data)

def replace_last_delimited_component(string, delimiter, replace_with):
    strings = string.split(delimiter)[:-1]
    final = ""
    for s in strings:
        final += s + delimiter
    return final + replace_with
    
# For use when the notebook is called from the command line.

"""
Expects the system arguments to come in the following form:
<Ax> <fx> <Ay> <fy> <phi> <delta_t> <N> <as_np_array> <save_to>
or, as either one or two file names. If only one is provided, the
output is written to a file of the same name, but of the .dat
extension type. If two are provided, it reads from the first file,
and writes to the second.
"""
if __name__ == '__main__':
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as input_file:
            args = input_file.readlines()
            args.append(
                replace_last_delimited_component(sys.argv[1], '.', 'dat')
            )
    elif len(sys.argv) == 3:
        with open(sys.argv[1], 'r') as input_file:
            args = input_file.readlines()
            args.append(sys.argv[2])
    else:
        args = sys.argv[1:]
    
    (Ax, fx) = (
        float(args[0]), float(args[1])
    )
    (Ay, fy, phi) = (
        float(args[2]), float(args[3]), float(args[4])
    )
    (delta_t, N, as_np_array) = (
        float(args[5]), int(args[6]), bool(args[7])
    )
    save_to = args[8]
    
    data = compute_sequence(delta_t, N, Ax, Ay, fx, fy, phi)
    if as_np_array:
        save_as_array(data, save_to)
    else:
        save_as_list(data, save_to)
    (t_out, x_out, y_out, z_out) = data
    plt.figure()
    plt.plot(x_out, y_out, '#ffb300')
    plt.xlabel('X(t) (unitless)')
    plt.ylabel('Y(t) (unitless)')
    plt.savefig(replace_last_delimited_component(args[8], '.', 'png'))